# `plantd` DAQ Device for a Raspberry Pi

[WIP] this project is pre-alpha and isn't expected to work correctly yet.

Uses the Measurement Computing DAQ hat for the Raspberry Pi.

## Development

### Without MCCDAQ Hardware

```shell
meson -Dwith-hardware=false _build
ninja -C _build
G_MESSAGES_DEBUG=all PLANTD_MODULE_CONFIGURATION=data/rpi-mccdaq.json _build/src/rpi-mccdaq -vvv
```

### With MCCDAQ Hardware

The configuration that's included in this project at `data/rpi-mccdaq.json` has `/dev/null` as the
file to load. This will need to be changed to a valid value that points to the hardware device. At
this time, that isn't known.

```shell
meson -Dwith-hardware=true _build
ninja -C _build
G_MESSAGES_DEBUG=all PLANTD_MODULE_CONFIGURATION=data/rpi-mccdaq.json _build/src/rpi-mccdaq -vvv
```

## Installation

Given that this service will be run on a Raspberry Pi, running this service is best done using a
`systemd` unit.

```shell
# build
meson -Dprefix=/usr -Dwith-hardware=true _build
ninja -C _build
sudo ninja -C _build install
# setup for systemd
sudo tee /usr/lib/systemd/system/plantd-module@.service > /dev/null <<'EOF'
[Unit]
Description=Plantd Module %I
Wants=network-online.target
After=network-online.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
RuntimeDirectory=plantd/modules
EnvironmentFile=-/etc/default/plantd/modules/%i
ExecStart=/usr/bin/plantd/modules/%i $PLANTD_MODULE_ARGS
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s HUP $MAINPID

[Install]
WantedBy=multi-user.target
EOF
# initialize environment
sudo tee /etc/default/plantd/modules/rpi-mccdaq > /dev/null <<'EOF'
G_MESSAGES_DEBUG=
PLANTD_MODULE_ARGS=
EOF
# enable and launch
sudo systemctl enable plantd-module@rpi-mccdaq
sudo systemctl start plantd-module@rpi-mccdaq
```
