#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "module.h"
#include "util.h"

struct _ApexRpiMccdaq
{
  ApexApplication      parent_instance;
  ApexConfiguration   *config;
  ApexRpiMccdaqDevice *daq;
};

G_DEFINE_TYPE (ApexRpiMccdaq, apex_rpi_mccdaq, APEX_TYPE_APPLICATION)

static ApexConfigurationResponse *
apex_rpi_mccdaq_get_configuration (ApexApplication  *self,
                                   GError          **error)
{
  ApexConfigurationResponse *response = NULL;
  ApexConfiguration *configuration = NULL;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ (self), NULL);

  g_debug ("handle `get-configuration' request");

  configuration = apex_configuration_new ();
  /* TODO: use a real ID, or none */
  apex_configuration_set_id (configuration, g_uuid_string_random ());
  apex_configuration_set_namespace (configuration, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);
  response = apex_configuration_response_new ();
  apex_configuration_response_set_configuration (response, configuration);

  return response;
}

static ApexStatusResponse *
apex_rpi_mccdaq_get_status (ApexApplication  *self,
                            GError          **error)
{
  g_autoptr (ApexStatus) status = NULL;
  ApexStatusResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ (self), NULL);

  g_debug ("handle `get-status' request");

  status = apex_status_new ();
  apex_status_set_active (status, TRUE);
  apex_status_set_loaded (status, TRUE);
  apex_status_set_enabled (status, TRUE);
  apex_status_add_detail (status,
                          "connected",
                          apex_rpi_mccdaq_device_is_connected (APEX_RPI_MCCDAQ (self)->daq));
  apex_status_add_detail (status,
                          "acquiring",
                          apex_rpi_mccdaq_device_is_acquiring (APEX_RPI_MCCDAQ (self)->daq));

  response = apex_status_response_new ();
  apex_status_response_set_status (response, status);

  return response;
}

static ApexSettingsResponse *
apex_rpi_mccdaq_get_settings (ApexApplication  *self,
                              GError          **error)
{
  ApexSettingsResponse *response;
  // ApexRpiMccdaq *app = NULL;
  g_autofree gchar *json = NULL;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ (self), NULL);

  g_debug ("handle `get-settings' request");

  // app = APEX_RPI_MCCDAQ (self);

  response = apex_settings_response_new ();

  /* TODO: add settings to the response */

  return response;
}

static ApexJobResponse *
apex_rpi_mccdaq_submit_job (ApexApplication  *self,
                            const gchar      *job_name,
                            const gchar      *job_value,
                            GHashTable       *job_properties,
                            GError          **error)
{
  g_autofree gchar *json = NULL;
  g_autoptr (ApexJob) job = NULL;
  ApexRpiMccdaq *app = NULL;
  ApexJobResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ (self), NULL);

  g_debug ("handle `submit-job' request");
  g_debug ("`%s' job requested", job_name);

  app = APEX_RPI_MCCDAQ (self);
  job = apex_rpi_mccdaq_job_new (app->daq, job_name, job_value);
  json = apex_job_serialize (job);

  response = apex_job_response_new ();
  apex_job_response_set_job (response, APEX_JOB (job));

  return response;
}

static void
apex_rpi_mccdaq_finalize (GObject *object)
{
  ApexRpiMccdaq *self = (ApexRpiMccdaq *)object;

  g_object_unref (self->daq);

  G_OBJECT_CLASS (apex_rpi_mccdaq_parent_class)->finalize (object);
}

static void
apex_rpi_mccdaq_class_init (ApexRpiMccdaqClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_rpi_mccdaq_finalize;

  APEX_APPLICATION_CLASS (klass)->get_configuration = apex_rpi_mccdaq_get_configuration;
  APEX_APPLICATION_CLASS (klass)->get_status = apex_rpi_mccdaq_get_status;
  APEX_APPLICATION_CLASS (klass)->get_settings = apex_rpi_mccdaq_get_settings;
  APEX_APPLICATION_CLASS (klass)->submit_job = apex_rpi_mccdaq_submit_job;

  g_assert (APEX_APPLICATION_CLASS (klass)->get_configuration != NULL);
}

static void
apex_rpi_mccdaq_init (ApexRpiMccdaq *self)
{
  g_autoptr (ApexProperty) property = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *config_file;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (APEX_IS_RPI_MCCDAQ (self));

  config_file = getenv_with_default ("PLANTD_MODULE_CONFIGURATION",
                                     "/etc/plantd/modules/rpi-mccdaq.json");
  g_message ("loading configuration from %s", config_file);
  self->config = apex_configuration_new ();
  apex_configuration_load (self->config, config_file, &error);

  // a device file is required, might need to handle this gracefully at some point
  g_assert_true (apex_configuration_has_property (self->config, "file"));

  property = apex_configuration_lookup_property (self->config, "file");
  apex_application_add_property (APEX_APPLICATION (self), property);
  self->daq = apex_rpi_mccdaq_device_new (apex_property_get_value (property));

  apex_application_add_property (APEX_APPLICATION (self), apex_property_new ("connectable", "true"));
  apex_application_add_property (APEX_APPLICATION (self), apex_property_new ("status", "ok"));
  apex_application_add_property (APEX_APPLICATION (self), apex_property_new ("display-name", "RPi MCC DAQ"));
  /* FIXME there may be a problem with libplantd that doesn't add the last property */
  apex_application_add_property (APEX_APPLICATION (self), apex_property_new ("dummy", "true"));
}

ApexRpiMccdaq *
apex_rpi_mccdaq_new (void)
{
  return g_object_new (APEX_TYPE_RPI_MCCDAQ, NULL);
}
