#ifndef APEX_RPI_MCCDAQ_DEVICE_H
#define APEX_RPI_MCCDAQ_DEVICE_H

#include <apex/apex.h>
#include <glib.h>

G_BEGIN_DECLS

#define APEX_TYPE_RPI_MCCDAQ_DEVICE apex_rpi_mccdaq_device_get_type ()
G_DECLARE_FINAL_TYPE (ApexRpiMccdaqDevice,
                      apex_rpi_mccdaq_device,
                      APEX,
                      RPI_MCCDAQ_DEVICE,
                      ApexApplication)

ApexRpiMccdaqDevice *apex_rpi_mccdaq_device_new               (const gchar         *file);

void                 apex_rpi_mccdaq_device_connect           (ApexRpiMccdaqDevice *self);
void                 apex_rpi_mccdaq_device_disconnect        (ApexRpiMccdaqDevice *self);
void                 apex_rpi_mccdaq_device_reset             (ApexRpiMccdaqDevice *self);

void                 apex_rpi_mccdaq_device_start_acquisition (ApexRpiMccdaqDevice *self);
void                 apex_rpi_mccdaq_device_stop_acquisition  (ApexRpiMccdaqDevice *self);

gboolean             apex_rpi_mccdaq_device_is_connected      (ApexRpiMccdaqDevice *self);
gboolean             apex_rpi_mccdaq_device_is_acquiring      (ApexRpiMccdaqDevice *self);

const gchar         *apex_rpi_mccdaq_device_get_file          (ApexRpiMccdaqDevice *self);
void                 apex_rpi_mccdaq_device_set_file          (ApexRpiMccdaqDevice *self,
                                                               const gchar         *file);

G_END_DECLS

#endif /* APEX_RPI_MCCDAQ_DEVICE_H */
