#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-unix.h>
#include <apex/apex.h>

#include "module.h"
#include "util.h"

static void
activate (GApplication *application)
{
  ApexApplication *app;
  ApexSource *events;

  app = APEX_APPLICATION (application);

  if (apex_application_has_source (app, "event"))
    {
      g_debug ("starting events source");
      events = apex_application_get_source (app, "event");
      apex_source_start (events);
    }
}

static gboolean
shutdown_cb (gpointer user_data)
{
  if (user_data == NULL)
    return FALSE;

  ApexApplication *app = (ApexApplication *)user_data;

  g_message ("signal received, shutting down");

  g_clear_object (&app);

  return G_SOURCE_REMOVE;
}

gint
main (gint argc, gchar *argv[])
{
  gint status;
  const gchar *endpoint;
  g_autoptr (ApexRpiMccdaq) app = NULL;
  g_autoptr (ApexSource) events = NULL;

  if (argc > 1 && g_strcmp0 (argv[1], "version") == 0)
    {
      g_print ("%s\n", VERSION);
      return 0;
    }

  apex_log_init (TRUE, NULL);

  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();

  app = apex_rpi_mccdaq_new ();

  // FIXME: these don't capture ctrl+c
  g_unix_signal_add (SIGHUP, shutdown_cb, app);
  g_unix_signal_add (SIGINT, shutdown_cb, app);

  endpoint = getenv_with_default ("PLANTD_MODULE_ENDPOINT", "tcp://localhost:7200");

  apex_application_set_id (APEX_APPLICATION (app), "org.plantd.RpiMccdaq");
  apex_application_set_endpoint (APEX_APPLICATION (app), endpoint);
  apex_application_set_service (APEX_APPLICATION (app), "rpi-mccdaq");

  g_application_set_inactivity_timeout (G_APPLICATION (app), 10000);

  events = apex_source_new ("tcp://localhost:11001", "");
  apex_application_add_source (APEX_APPLICATION (app), "event", events);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

  status = g_application_run (G_APPLICATION (app), argc, argv);

  apex_log_shutdown ();

  return status;
}
