#ifndef RPI_MCCDAQ_H
#define RPI_MCCDAQ_H

#include <glib.h>
#include <gio/gio.h>
#include <apex/apex.h>

#include "job.h"

G_BEGIN_DECLS

#define APEX_TYPE_RPI_MCCDAQ apex_rpi_mccdaq_get_type ()
G_DECLARE_FINAL_TYPE (ApexRpiMccdaq, apex_rpi_mccdaq, APEX, RPI_MCCDAQ, ApexApplication)

ApexRpiMccdaq *apex_rpi_mccdaq_new (void);

G_END_DECLS

#endif /* RPI_MCCDAQ_H */
