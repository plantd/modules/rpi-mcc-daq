#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "job.h"

struct _ApexRpiMccdaqJob {
  GObject              parent;

  ApexRpiMccdaqDevice *daq;
  gchar               *job_name;
  gchar               *job_value;
};

enum {
  PROP_0,
  PROP_DAQ,
  PROP_JOB_NAME,
  PROP_JOB_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexRpiMccdaqJob, apex_rpi_mccdaq_job, APEX_TYPE_JOB)

static void
apex_rpi_mccdaq_job_finalize (GObject *object)
{
  ApexRpiMccdaqJob *self = (ApexRpiMccdaqJob *)object;

  g_object_unref (self->daq);
  g_clear_pointer (&self->job_name, g_free);
  if (self->job_value != NULL)
    g_clear_pointer (&self->job_value, g_free);

  G_OBJECT_CLASS (apex_rpi_mccdaq_job_parent_class)->finalize (object);
}

static void
apex_rpi_mccdaq_job_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  ApexRpiMccdaqJob *self = APEX_RPI_MCCDAQ_JOB (object);

  switch (prop_id)
    {
    case PROP_DAQ:
      g_value_set_object (value, self->daq);
      break;

    case PROP_JOB_NAME:
      g_value_set_string (value, self->job_name);
      break;

    case PROP_JOB_VALUE:
      g_value_set_string (value, self->job_value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_rpi_mccdaq_job_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  ApexRpiMccdaqJob *self = APEX_RPI_MCCDAQ_JOB (object);

  switch (prop_id)
    {
    case PROP_DAQ:
      self->daq = g_value_get_object (value);
      break;

    case PROP_JOB_NAME:
      g_clear_pointer (&self->job_name, g_free);
      self->job_name = g_value_dup_string (value);
      break;

    case PROP_JOB_VALUE:
      g_free (self->job_value);
      self->job_value = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_rpi_mccdaq_job_task (ApexJob  *self,
                          gpointer  data)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_JOB (self));

  g_debug ("job name: %s\n", APEX_RPI_MCCDAQ_JOB (self)->job_name);

  if (g_strcmp0 (APEX_RPI_MCCDAQ_JOB (self)->job_name, "connect") == 0)
    apex_rpi_mccdaq_device_connect (APEX_RPI_MCCDAQ_JOB (self)->daq);
  else if (g_strcmp0 (APEX_RPI_MCCDAQ_JOB (self)->job_name, "disconnect") == 0)
    apex_rpi_mccdaq_device_disconnect (APEX_RPI_MCCDAQ_JOB (self)->daq);
  else if (g_strcmp0 (APEX_RPI_MCCDAQ_JOB (self)->job_name, "start-acquisition") == 0)
    apex_rpi_mccdaq_device_start_acquisition (APEX_RPI_MCCDAQ_JOB (self)->daq);
  else if (g_strcmp0 (APEX_RPI_MCCDAQ_JOB (self)->job_name, "stop-acquisition") == 0)
    apex_rpi_mccdaq_device_stop_acquisition (APEX_RPI_MCCDAQ_JOB (self)->daq);
  else
    g_warning ("invalid job name: %s", APEX_RPI_MCCDAQ_JOB (self)->job_name);
}

static void
apex_rpi_mccdaq_job_class_init (ApexRpiMccdaqJobClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_rpi_mccdaq_job_finalize;
  object_class->get_property = apex_rpi_mccdaq_job_get_property;
  object_class->set_property = apex_rpi_mccdaq_job_set_property;

  APEX_JOB_CLASS (klass)->task = apex_rpi_mccdaq_job_task;

  properties [PROP_DAQ] =
    g_param_spec_object ("daq",
                         "DAQ",
                         "The DAQ device to use in the job task",
                         APEX_TYPE_RPI_MCCDAQ_DEVICE,
                         (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_JOB_NAME] =
    g_param_spec_string ("job-name",
                         "JobName",
                         "The name of the specific job task",
                         NULL,
                         (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_JOB_VALUE] =
    g_param_spec_string ("job-value",
                         "JobValue",
                         "The value of the specific job task",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_rpi_mccdaq_job_init (ApexRpiMccdaqJob *self)
{
  g_object_ref (self->daq);
}

ApexJob *
apex_rpi_mccdaq_job_new (ApexRpiMccdaqDevice *daq,
                         gchar               *job_name,
                         gchar               *job_value)
{
  g_return_val_if_fail (daq != NULL, NULL);
  g_return_val_if_fail (job_name != NULL, NULL);

  return g_object_new (APEX_TYPE_RPI_MCCDAQ_JOB,
                       "daq",       daq,
                       "job-name",  job_name,
                       "job-value", job_value,
                       NULL);
}
