#ifndef RPI_MCCDAQ_UTIL_H
#define RPI_MCCDAQ_UTIL_H

#include <glib.h>

G_BEGIN_DECLS

const gchar *getenv_with_default (const gchar *env, const gchar *default_env);

G_END_DECLS

#endif /* RPI_MCCDAQ_UTIL_H */
