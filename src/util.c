#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "util.h"

const gchar *
getenv_with_default (const gchar *env, const gchar *default_env)
{
  if (g_getenv (env) == NULL)
    return g_strdup (default_env);
  return g_getenv (env);
}
