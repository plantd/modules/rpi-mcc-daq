#!/bin/bash

# required: gnome-c-utils

# lineup parameters
find . -name "*.c" | parallel gcu-lineup-parameters
find . -name "*.h" | parallel gcu-lineup-parameters

# align params
find . -name "*.c" | parallel gcu-align-params-on-parenthesis
find . -name "*.h" | parallel gcu-align-params-on-parenthesis

# ensure config.h is included in c
find . -name "*.c" | parallel gcu-include-config-h

# test virtual function chain-ups
find . -name "*.c" | parallel gcu-check-chain-ups
