project('rpi-mccdaq', ['c'],
  default_options: [
    'buildtype=debugoptimized',
  ],
  license: 'proprietary',
  meson_version: '>= 0.51.0'
)

r = run_command('git', 'describe')
if r.returncode() != 0
    # it failed
endif

rpi_mccdaq_version = r.stdout().strip().split('v')[1].split('-')[0]
version_array = rpi_mccdaq_version.split('.')
rpi_mccdaq_major_version = version_array[0].to_int()
rpi_mccdaq_minor_version = version_array[1].to_int()
rpi_mccdaq_micro_version = version_array[2].to_int()

rpi_mccdaq_ns = 'Apex'
rpi_mccdaq_buildtype = get_option('buildtype')

rpi_mccdaq_prefix = get_option('prefix')
rpi_mccdaq_datadir = join_paths(rpi_mccdaq_prefix, get_option('datadir'))
rpi_mccdaq_includedir = join_paths(rpi_mccdaq_prefix, get_option('includedir'))

global_args = [
  '-DHAVE_CONFIG_H',
  '-I' + meson.build_root(),
]

if get_option('with-hardware')
  global_args += '-DHAVE_HARDWARE'
endif

add_global_arguments(global_args, language: 'c')

cc = meson.get_compiler('c')

test_cflags = [
  '-ffast-math',
  '-fstrict-aliasing',
  '-Wpointer-arith',
  '-Wmissing-declarations',
  '-Wformat=2',
  '-Wstrict-prototypes',
  '-Wmissing-prototypes',
  '-Wnested-externs',
  '-Wold-style-definition',
  '-Wdeclaration-after-statement',
  '-Wunused',
  '-Wuninitialized',
  '-Wshadow',
  '-Wmissing-noreturn',
  '-Wmissing-format-attribute',
  '-Wredundant-decls',
  '-Wlogical-op',
  '-Wcast-align',
  '-Wno-unused-local-typedefs',
  '-Werror=implicit',
  '-Werror=init-self',
  '-Werror=main',
  '-Werror=missing-braces',
  '-Werror=return-type',
  '-Werror=array-bounds',
  '-Werror=write-strings'
]

common_flags = cc.get_supported_arguments(test_cflags)

if rpi_mccdaq_buildtype.contains('debug')
  common_flags += [ '-DRPI_MCCDAQ_ENABLE_DEBUG' ]

  if rpi_mccdaq_buildtype.contains('optimized')
    common_flags += [ '-DG_DISABLE_CAST_CHECKS' ]
  endif
else
  common_flags += [
    '-DG_DISABLE_CAST_CHECKS',
    '-DG_DISABLE_CHECKS',
    '-DG_DISABLE_ASSERT'
  ]
endif

add_project_arguments(common_flags, language: 'c')

extra_args= []

core_inc = include_directories('.')

dep_glib = dependency('glib-2.0')
dep_gio = dependency('gio-2.0')
dep_gobject = dependency('gobject-2.0')
dep_zmq = dependency('libzmq')
dep_czmq = dependency('libczmq')
dep_apex = dependency('libapex-1.0')

rpi_mccdaq_deps = [
  dep_glib,
  dep_gio,
  dep_gobject,
  dep_zmq,
  dep_czmq,
  dep_apex,
]

dir_sysconf = join_paths(get_option('sysconfdir'), 'plantd/modules')
subst = configuration_data()
subst.set('sysconfdir', dir_sysconf)

subdir('src')
subdir('data')
